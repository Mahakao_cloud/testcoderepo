<?php
/**
 * Created by PhpStorm.
 * User: odenysiuk
 * Date: 14.06.18
 * Time: 11:25
 */

namespace AppBundle\Controller;

use AppBundle\Entity\DrugList;
use AppBundle\Form\DrugListForm;
use AppBundle\Libs\JsonResponse\ApiProblem;
use AppBundle\Libs\JsonResponse\ApiResponse;
use FOS\UserBundle\Model\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class DrugController extends Controller
{

    /**
     * @return Response
     *
     * wyświetlanie listy leków
     */
    public function drugIndexAction(): Response
    {
        $user = $this->getUser();

        if (!($user instanceof User)) {
            return new RedirectResponse($this->generateUrl('homepage'));
        }

        $drugList = $this->getDoctrine()
            ->getRepository('AppBundle:DrugList')
            ->findAll();

        return $this->render('@App/patient/patientDrugList.html.twig', [
            'drug_list' => $drugList
        ]);
    }

    /**
     * @return JsonResponse
     *
     * lista leków jako JSON
     */
    public function drugListAction(): JsonResponse
    {
        $user = $this->getUser();

        if (!($user instanceof User)) {

            return (new ApiProblem())
                ->setTitle('Wymagane logowanie')
                ->setStatus(Response::HTTP_UNAUTHORIZED)
                ->setDetail('Dostęp ograniczony, zaloguj się')
                ->getResponse();
        }

        $drugList = $this->getDoctrine()
            ->getRepository('AppBundle:DrugList')
            ->findAll();

        $responseDrugList = [];

        //API
        $response = (new ApiResponse())
            ->setStatus(Response::HTTP_OK);

        foreach ($drugList as $drugListToResponse) {
            $responseDrugList[] = [
                'drug_name' => $drugListToResponse->getDrugName(),
                'drug_active_substance' => $drugListToResponse->getDrugActiveSubstance(),
                'drug_active_substance_dose' => $drugListToResponse->getDrugActiveSubstanceDose(),
                'drug_form' => $drugListToResponse->getDrugForm(),
            ];
        }

        return $response
            ->setData($responseDrugList)
            ->getResponse();

    }


    /**
     * @param Request $request
     * @return Response
     *
     * formularz dodawania leków
     */
    public function addToDrugListAction(Request $request): Response
    {
        $user = $this->getUser();

        if (!($user instanceof User)) {
            return new RedirectResponse($this->generateUrl('homepage'));
        }

        if (!$this->isGranted('ROLE_PATIENT_MANAGER')) {
            return $this->render('@App/errors/403.html.twig', [
                'error_message' => "Brak wystarczających uprawnień"
            ]);
        }

        $drugList = new DrugList();

        $form = $this->createForm(DrugListForm::class, $drugList);
        $form->setData($drugList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($drugList);
            $entityManager->flush();

            return $this->redirectToRoute('patient_add_drug_list', [
                'drug_name' => $drugList->getDrugName(),
                'drug_active_substance' => $drugList->getDrugActiveSubstance(),
                'drug_active_substance_dose' => $drugList->getDrugActiveSubstanceDose(),
                'drug_form' => $drugList->getDrugForm()
            ]);
        }

        $responseData = [
            'drug_list_form' => $form->createView()
        ];

        return $this->render('@App/patient/patientAddDrugList.html.twig', $responseData);
    }

}
